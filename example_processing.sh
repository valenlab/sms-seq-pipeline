#!/bin/bash

# Annotates fast5 files with basecalled sequence
tombo preprocess annotate_raw_with_fastqs --fast5-basedir "./example_data/fast5/treated" --fastq-filenames "./example_data/fastq/treated_500subset.fastq" --processes 1 --overwrite
tombo preprocess annotate_raw_with_fastqs --fast5-basedir "./example_data/fast5/control" --fastq-filenames "./example_data/fastq/control_1000subset.fastq" --processes 1 --overwrite

# Performs resquiggle step for each of the folders
# In the paper --max-scaling-iterations was set to 25
tombo resquiggle "./example_data/fast5/treated" "./reference.fa" --processes 4 --num-most-common-errors 5 --rna --overwrite --max-scaling-iterations 25 --failed-reads-filename untreated_failed --include-event-stdev
tombo resquiggle "./example_data/fast5/control" "./reference.fa" --processes 4 --num-most-common-errors 5 --rna --overwrite --max-scaling-iterations 25 --failed-reads-filename untreated_failed --include-event-stdev

# Finally, compare treated and control
tombo detect_modifications model_sample_compare --fast5-basedirs "./example_data/fast5/treated" \
    --control-fast5-basedirs "./example_data/fast5/control" --rna --statistics-file-basename tombo_output \
    --per-read-statistics "tombo_output" --fishers-method-context 3

# use 1_p_val_to_csv script to extract tombo statistics to csv file
# 1 382 are start and stop postions on the reference for extraction
python ./1_p_val_to_csv.py './tombo_output.tombo.per_read_stats' 'pCS2haRNAmedium' 1 382 './output_file.csv'


# This example dataset is in no sense biologically meaningfull, it just documents how we processed the data
# for the genome alignments you can use same processing, but for statistic extraction use 1_p_val_to_csv_genome.py

# Print output
echo "FINISHED PROCESSING!"
echo "head ./output_file.csv"
head ./output_file.csv
