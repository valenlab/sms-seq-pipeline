from tombo import tombo_helper, tombo_stats
import numpy as np
import pandas as pd
import sys
import os
from numpy.lib.recfunctions import append_fields
from Bio import SeqIO

# remember to remove .csv file before running!

stat_file = os.path.abspath(sys.argv[1]) # per read_stats file!
fa_file = os.path.abspath(sys.argv[2])
out_name = sys.argv[3] # output file

prs = tombo_stats.PerReadStats(stat_file)

for record in SeqIO.parse(fa_file, "fasta"):
    int_data = tombo_helper.intervalData(chrm=record.id, start=0, end=len(record.seq), strand='+')
    stats = prs.get_region_per_read_stats(int_data, num_reads=None)
    if stats is not None and stats.size > 1:
        stats = append_fields(stats, 'seqnames', np.full(stats.size, record.id.split("|")[0]))
        stats = append_fields(stats, 'gene', np.full(stats.size, record.id.split("|")[1]))
        stats = append_fields(stats, 'biotype', np.full(stats.size, record.id.split("|")[7]))
        df = pd.DataFrame.from_records(stats)
        df.to_csv(out_name, mode='a', header=False, index=False)
