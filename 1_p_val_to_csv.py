# args: 
# 1. path to .per_read_stats
# 2. name of the fasta header
# 3. positions for extraction of reads start # 0-based start position
# 4. positions for extraction of reads stop # 1-based (or open interval) end position - not inclusive 
# 5. path to output file with .csv extension

from tombo import tombo_helper, tombo_stats
import numpy as np
import pandas as pd
import sys
import os
from numpy.lib.recfunctions import append_fields

stat_file = os.path.abspath(sys.argv[1])
name = sys.argv[2]
out_name = sys.argv[5]
start = int(sys.argv[3]) 
stop = int(sys.argv[4])

# per read testing
prs = tombo_stats.PerReadStats(stat_file)
bp = stop - start
int_data = tombo_helper.intervalData(chrm=name, start=start, end=stop, strand='+')
stats = prs.get_region_per_read_stats(int_data)
df = pd.DataFrame.from_records(stats)
df.to_csv(out_name, index=False)
