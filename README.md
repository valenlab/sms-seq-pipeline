## About 

This is an example pipeline for SMS-seq processing of the nanopore data.  
In the example folder you can find example ONT Nanopore basecalled reads for treated (with DEPC) and control (no DEPC).

## Instructions  

Install conda form [here](https://docs.conda.io/en/latest/).  
Make sure conda is working. 
Run from inside of this directory to install dependencies for conda environment:

```
conda env create -f environment.yml 
```

Now you can run example bash script with that conda environment. If you don't have bash you can run each command line by line from the example_processing.sh file instead, otherwise:

```
conda activate SMS_seq
./example_processing.sh
conda deactivate
```

## Output

As output you will get a "output_file.csv" table containing tombo statistic values indicating modification rate for each position on the example `./reference.fa` sequence.

